import { Component, ViewChild, ElementRef } from '@angular/core';
import { ToastController, NavController } from 'ionic-angular';
import {createViewChild} from "@angular/compiler/src/core";

import { HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import { Socket } from 'ng-socket-io';


declare var google:any;

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})

export class AboutPage {

  @ViewChild('map') mapRef: ElementRef;
  data: Observable<any>;
  constructor(private toast: ToastController,
              public navCtrl: NavController,
              public http: HttpClient,
              private socket: Socket) {

  }
  ionViewDidLoad(){
    this.DisplayMap();
  }

  DisplayMap(){
    const location = new google.maps.LatLng('14.602203', '120.9864577');

    const options = {
      center:location,
        zoom: 17,
        mapTypeControl: false,
        disableDefaultUI: true

    };
      const map = new google.maps.Map(this.mapRef.nativeElement,options);

      var marker = new google.maps.Marker({
          position: location,
          map: map,
          title: 'Vehicle Location'
      });

      // setInterval(function() {
      //     location = new google.maps.LatLng('14.6','120.99');
      //     marker.setPosition(location);
      //     map.setCenter(location);
      // }, 10000);
  }


  UnlockVehicle(){
      let toast = this.toast.create({
          message: 'Command Successfully Executed',
          duration: 3000,
          position: 'top'
      });

      this.socket.connect();
      this.socket.emit("light",1);
      console.log(1);
  }

  LockVehicle(){
      this.socket.connect();
      this.socket.emit("light",0);
      console.log(0);

      let toast = this.toast.create({
          message: 'Command Successfully Executed',
          duration: 3000,
          position: 'top'
      });

      toast.present();
  }
}
